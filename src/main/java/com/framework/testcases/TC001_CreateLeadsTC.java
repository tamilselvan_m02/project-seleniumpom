package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_CreateLeadsTC extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		
		testCaseName="TC001_LoginLogout";
		testDescription="Login and Logout Leaftaps";
		testNodes="Leads";
		author="Tamil";
		category="smoke";
		dataSheetName="TC001";
			
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password, String companyname,String firstname, String lastname) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickcrmsfa()
		.clickLeads()
		.clickCreateLeads()
		.enterCompanyName(companyname)
		.enterFirstname(firstname)
		.enterLastname(lastname)
		.clickCreateLead();
	}

}
