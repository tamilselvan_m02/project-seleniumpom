package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "createLeadForm_firstName") WebElement elefirstname;

	public ViewLeadPage verifyfirstname(String data) {
		// WebElement eleUsername = locateElement("id", "username");
		verifyPartialText(elefirstname, data);
		return this;
	}



}
