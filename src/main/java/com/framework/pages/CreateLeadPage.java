package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	
		public CreateLeadPage() {
		       PageFactory.initElements(driver, this);
			}
			
			@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement elecomname;
			@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement elefirstname;
			@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement elelastname;
			@FindBy(how = How.NAME,using="submitButton") WebElement elecreatlead;
			
			public CreateLeadPage enterCompanyName(String data) {
				//WebElement eleUsername = locateElement("id", "username");
				clearAndType(elecomname, data);
				return this; 
			}
			public CreateLeadPage enterFirstname(String data) {
				//WebElement elePassword = locateElement("id", "password");
				clearAndType(elefirstname, data);
				return this;
			}
			public CreateLeadPage enterLastname(String data) {
				//WebElement elePassword = locateElement("id", "password");
				clearAndType(elelastname, data);
				return this;
			}
			public ViewLeadPage clickCreateLead() {
				//WebElement eleLogin = locateElement("class", "decorativeSubmit");
			    click(elecreatlead);
			    /*HomePage hp = new HomePage();
			    return hp;*/ 
			    return new ViewLeadPage();
			}
	}

